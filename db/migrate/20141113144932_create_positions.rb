class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :position_name
      t.string :mrc
      t.integer :user_id

      t.timestamps
    end
    add_index :positions, [:user_id, :created_at]
  end
end
