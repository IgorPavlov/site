class Position < ActiveRecord::Base
	belongs_to :user
	
	
	validates :position_name, presence: true, :nullify => false
	validates :mrc, presence: true, :nullify => false
	validates :user_id, presence: true
end
