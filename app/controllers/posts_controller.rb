class PostsController < ApplicationController
  require 'roo'

  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  

  def create
    
    
    @post = current_user.posts.build(post_params)
    
    if @post.save
      @post_del = Post.where(user_id: current_user[:id]).second
    
      unless @post_del.nil?
        @post_del.attachment = nil
        @post_del.destroy
      end
      
      flash[:success] = "Файл отправлен"
      excel_to_db
     
    else
      flash[:error] = "Ошибка: выберите файл с указаным форматом"
    end
    
    redirect_to root_url
    

  end

  def destroy
    @post.attachment = nil
    @post.destroy
    redirect_to root_url
  end

  private

    def post_params
      params.require(:post).permit(:content, :attachment)
    end

    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end

    def excel_to_db
      
      workbook = Roo::Spreadsheet.open("public/#{@current_user.posts.first.attachment}")


      inserts = []
    
      3.upto(workbook.last_row) do |line|
        position_name = workbook.cell(line,'B')
        mrc = workbook.cell(line,'C')
        inserts.push "('#{position_name.mb_chars.upcase.strip}', '#{mrc}', '#{current_user.id}','#{Time.now}')" unless position_name.nil?
      end
      join = inserts.sort.join(', ')
      sql = "INSERT INTO positions (position_name, mrc, user_id, created_at) VALUES #{join}"
      Position.delete_all("user_id = #{@current_user.id}")
      ActiveRecord::Base.connection.execute sql
      clients_mail_push
      
    end

    def clients_mail_push
      @current_user.followers.each do |client|
        Spreadsheet.client_encoding = 'UTF-8'
        book = Spreadsheet::Workbook.new
        sheet1 = book.create_worksheet :name => 'test'
        i = 0
        Position.where(user_id: client.followed_users).order("position_name").each do |user|
          sheet1.row(i).push user.position_name, user.mrc
          i = i + 1
        end
        book.write 'public/test.xlsx'
        UserMailer.delay.welcome_email(client.email)
      end
    end

end