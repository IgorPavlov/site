class PositionsController < ApplicationController
 def create(id)
 	Position.where(:user_id => @current_user.id).destroy_all
      workbook = Roo::Spreadsheet.open("public/#{@current_user.posts.first.attachment}")
      
      3.upto(200) do |line| #start and end of row
        position_name = workbook.cell(line,'B')
        mrc = workbook.cell(line,'C')

        @position = Position.create(:position_name => position_name,:mrc => mrc,:user_id => id)
      end
  end
end
