class StaticPagesController < ApplicationController
  def home
    if signed_in?
  	  @post = @current_user.posts.build
  	  @posts = current_user.feed.paginate(page: params[:page])
  	  @users = User.limit(20)
    end
  	
  end

end
