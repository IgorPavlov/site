class UsersController < ApplicationController
  before_action :signed_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]

  def index
    @users = User.paginate(page: params[:page])
  end
  
  def new
  	@user = User.new
  end

  def show
  	@user = User.where(id: params[:id]).first
    @posts = @user.posts.paginate(page: params[:page])
    @post = current_user.posts.build if signed_in?
  end

  def create
  	@user = User.new(user_params)
    if @user.save
      
      sign_in @user
      flash[:success] = "Добро пожаловать!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

private

def user_params
	params.require(:user).permit(:name, :email, :password, :password_confirmation)
end

# Before filters

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
