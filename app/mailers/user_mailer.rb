class UserMailer < ActionMailer::Base
	
  default :from => 'admin@mrc-now.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def welcome_email(email)
  	
    attachments.inline['test.xlsx'] = File.read('public/test.xlsx')
    mail( :to => email,
    :subject => 'Новые цены',
     )
  end
end
